//Câu lệnh này tương tự import express from 'express'; Dùng import thư viện vào project
const express = require("express");

//Khai báo thư viện Mongoose
const mongoose = require('mongoose');

//Kết nối đến cơ sở dữ liệu Mongoose
mongoose.connect('mongodb://localhost:27017/CRUD_Pizza365', (error) => {
    if(error) {
        throw error;
    }
    console.log("Successfully connected!");
});

//Import Router
const drinkRouter = require("./app/routers/drinkRouters");
const voucherRouter = require("./app/routers/voucherRouter");
const userRouter = require("./app/routers/userRouter");
const orderRouter = require("./app/routers/orderRouter");

//Khai báo các model
const drinkModel = require("./app/models/drinkModel");
const voucherModel = require("./app/models/voucherModel");
const orderModel = require("./app/models/orderModel");
const userModel = require("./app/models/userModel");

//Khai báo thư viện path
const path = require("path");

//Khởi tạo express
const app = express();

//Khai báo cổng của project
const port = 8000;

//Khai báo sử dụng các tài nguyên static (image, js, css, v...v...)
app.use(express.static("views"));

//Sử dụng được body json
app.use(express.json());

//Sử dụng body unicode 
app.use(express.urlencoded({
    extended:true
}))

//Viết middelware console ra thời gian hiện tại mỗi lần chạy
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
})

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Call back function là một tham số của hàm khác và nó sẽ được thực thi  ngay sau khi hàm đấy được gọi
app.get("/index", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"));
})

//Sử dụng Router
app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})