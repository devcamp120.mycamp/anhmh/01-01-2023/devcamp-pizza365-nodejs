//Import Drink Model 
const { response, request } = require('express');
const { default: mongoose } = require('mongoose');
const drinkModel = require('../models/drinkModel');

//Khai báo các Actions:
//Create a drink
const createDrink = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!body.maNuocUong) {
        response.status(400).json({
            message: "[maNuocUong] is required!"
        })
    } else if (!body.tenNuocUong) {
        response.status(400).json({
            message: "[tenNuocUong] is required!"
        })
    } else if (!Number.isInteger(body.donGia) || body.donGia < 0) {
        response.status(400).json({
            message: "[donGia] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let drink = {
            maNuocUong: body.maNuocUong,
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        }
        drinkModel.create(drink, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(201).json({
                    data
                })
            }
        })
    }
}

//Get all drink
const getAllDrink = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    drinkModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(200).json({
                data
            })
        }
    })
};

//Get a drink by id
const getDrinkById = (request, response) => {
    //Bước 1: Thu thập dữ liệu (bỏ qua)
    let id = request.params.drinkId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        drinkModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};



//Update a drink
const updateDrinkById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.drinkId;
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu 
    if(!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else if (!body.maNuocUong) {
        response.status(400).json({
            message: "[maNuocUong] is required!"
        })
    } else if (!body.tenNuocUong) {
        response.status(400).json({
            message: "[tenNuocUong] is required!"
        })
    } else if(!Number.isInteger(body.donGia) || body.donGia < 0) {
        response.status(400).json({
            message: "[donGia] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác nghiệp vụ
        let drink = {
            maNuocUong: body.maNuocUong,
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        }
        drinkModel.findByIdAndUpdate(id, drink, (error,data)=> {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
}

//Delete a drink
const deleteDrinkById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.drinkId;

    //Bước 2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3; Thực hiện các thao tác nghiệp vụ
        drinkModel.findByIdAndDelete(id,(error,data) => {
            if(error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        })
    }
};
//Export các hàm ra module
module.exports = { getAllDrink, getDrinkById, createDrink, updateDrinkById, deleteDrinkById }