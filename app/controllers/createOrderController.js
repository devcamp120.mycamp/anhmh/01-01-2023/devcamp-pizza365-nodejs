const { response } = require("express");
const mongoose = require("mongoose");

const drinkModel = require("../models/drinkModel");
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");
const voucherModel = require("../models/voucherModel");

const createOrder = (request, response) => {
    //Bước 1: Chuẩn bị dữ liệu
    let fullName = request.body.fullName;
    let email = request.body.email;
    let address = request.body.address;
    let phone = request.body.phone;

    //Bước 2: Validate dữ liệu từ request body
    if (!fullName) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Fullname is required"
        })
    }
    if (!email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Email is required"
        })
    }
    if (!address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Address is required"
        })
    }
    if (!phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Phone is required"
        })
    }

    //Sử dụng userModel tìm kiếm bằng email
    userModel.findOne({
        email: email
    }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            if (!userExist) {
                //Nếu user không tồn tại trong hệ thống
                //Tạo user mới
                userModel.create({
                    fullName: fullName,
                    email: email,
                    address: address,
                    phone: phone
                }, (errCreateUser, userCreated) => {
                    if (errCreateUser) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreateUser.message
                        })
                    } else {
                        //Nếu user đã tồn tại trong hệ thống, lấy id của user đó để tạo Order
                        orderModel.create({
                            user: userExist._id,
                            orderCode: orderCode,
                            pizzaSize: pizzaSize,
                            pizzaType: pizzaType
                        })
                    }
                })
            }
        }
    })
}

module.exports = {
    createOrder
}
