const mongoose = require("mongoose");
const { response, request } = require("express");

const userModel = require("../models/userModel");
const orderModel = require("../models/orderModel");

const createOrderofUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    }
    if (!requestBody.orderCode) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[orderCode] is invalid!"
        })
    }
    if (!requestBody.pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[pizzaSize] is invalid!"
        })
    }
    if (!requestBody.pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[pizzaType] is invalid!"
        })
    }
    if (!requestBody.status) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[status] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    let newOrderInput = {
        _id: mongoose.Types.ObjectId(),
        orderCode: requestBody.orderCode,
        pizzaSize: requestBody.pizzaSize,
        pizzaType: requestBody.pizzaType,
        status: requestBody.status,
        voucher: mongoose.Types.ObjectId(),
        drink: mongoose.Types.ObjectId()
    }
    console.log(newOrderInput)
    orderModel.create(newOrderInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { reviews: data._id }
                },
                (err, updatedOrder) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllOrderOfUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    //Bước 2: Kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.orders
                })
            }
        })

}

const getOrderById = (request, response) => {
    //Bước 1: Thu thập dữ liệu 
    let orderId = request.params.orderId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get order success",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let orderId = request.params.orderId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is invalid!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let orderUpdate = {
        orderCode: requestBody.orderCode,
        pizzaSize: requestBody.pizzaSize,
        voucher: requestBody.voucher,
        drink: requestBody.drink,
        status: requestBody.status
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update review success",
                data: data
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    //Bước 1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let orderId = request.params.orderId;

    //Bước 2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "userId is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is invalid!"
        })
    }
    //Bước 3: Thao tác với dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            //Sau khi xoá 1 order khỏi collection cần xoá thêm orderId trong user đang chứa nó
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { orders: orderId }

                },
                (error, updateUser) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete order success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

module.exports = {
    createOrderofUser: createOrderofUser,
    getAllOrderOfUser: getAllOrderOfUser,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById
}

