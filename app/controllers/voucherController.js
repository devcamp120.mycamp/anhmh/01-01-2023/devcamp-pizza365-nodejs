//Import voucher model
const { default: mongoose } = require('mongoose');
const voucherModel = require('../models/voucherModel');


//Khai báo các Actions:
//Create a voucher
const createVoucher = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if(!body.maVoucher) {
        response.status(400).json({
            message: "[maVoucher] is required!"
        })
    } else if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        response.status(400).json({
            message: "[phanTramGiamGia] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let voucher = {
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia
        }
        voucherModel.create(voucher, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(201).json({
                    data
                })
            }
        })
    }
};

//Get all vouchers
const getAllVoucher = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(200).json({
                data
            })
        }
    })
};

//Get a voucher by id
const getVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu (bỏ qua)
    let id = request.params.voucherId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        voucherModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};

//Update a voucher
const updateVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.voucherId;
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu 
    if(!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else if (!body.maVoucher) {
        response.status(400).json({
            message: "[maVoucher] is required!"
        })
    } else if(!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        response.status(400).json({
            message: "[donGia] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác nghiệp vụ
        let voucher = {
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia,
        }
        voucherModel.findByIdAndUpdate(id, voucher, (error,data)=> {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
}
//Delete a voucher
const deleteVoucherById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.voucherId;

    //Bước 2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3; Thực hiện các thao tác nghiệp vụ
        voucherModel.findByIdAndDelete(id,(error,data) => {
            if(error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        })
    }
};
//Export các hàm ra module
module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById }
