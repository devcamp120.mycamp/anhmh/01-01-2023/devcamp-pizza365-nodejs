//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const orderSchema = new Schema({
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Voucher"
        }
    ],
    drink: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Drink"
        }
    ],
    status: {
        type: String,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
    
});
//Bước 4: Export ra 1 model cho schema
module.exports = mongoose.model("Order", orderSchema);

/*
Order: {
	_id: ObjectId, unique
	orderCode: String, unique
	pizzaSize: String, required
	pizzaType: String, required
	voucher: ObjectID, ref: Voucher
    drink: ObjectID, ref: Drink
	status: String, required
ngayTao: Date, default: Date.now()
ngayCapNhat: Date, default: Date.now()
}

*/