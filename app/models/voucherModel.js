//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const voucherSchema = new Schema({
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Voucher", voucherSchema);

/*

Voucher: {
	_id: ObjectId, unique
	maVoucher: String, unique, required
	phanTramGiamGia: Number, required
	ghiChu: String, not required
	ngayTao: Date, default: Date.now()
ngayCapNhat: Date, default: Date.now()
}

*/