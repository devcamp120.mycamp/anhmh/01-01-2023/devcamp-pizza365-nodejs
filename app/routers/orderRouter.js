//Import bộ thư viện Express
const express = require('express');

const {  createOrderofUser, getAllOrderOfUser, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/orderController");
const { createOrder } = require("../controllers/createOrderController");

const routerOrder = express.Router();

routerOrder.post("/users/:userId/orders", createOrderofUser);

routerOrder.post("/devcamp-pizza365/orders", createOrder);

routerOrder.get("/users/:userId/", getAllOrderOfUser);

routerOrder.get("/orders/:orderId", getOrderById);

routerOrder.put("/orders/:orderId", updateOrderById);

routerOrder.delete("/users/:userId/orders/:orderId", deleteOrderById);

//Export dữ liệu thành 1 module
module.exports = routerOrder;