//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import module controllers
const { createUser, getAllUser, getAllUserById, updateUserById, deteleUserById, limitUsers, skipUsers, sortUsers, skipLimitUsers, sortSkipLimitUsers } = require("../controllers/userController");
const userRouter = express.Router();

//Create a user
userRouter.post("/users/", createUser);

//Limit users
userRouter.get("/limit-users", limitUsers);

//Skip users
userRouter.get("/skip-users", skipUsers);

//Sort users
userRouter.get("/sort-users", sortUsers);

//Skip limit users
userRouter.get("/skip-limit-users", skipLimitUsers);

//Sort skip limit users
userRouter.get("/sort-skip-limit-users", sortSkipLimitUsers);

//Get a user
userRouter.get("/users", getAllUser);

//Get a user by id
userRouter.get("/users/:userId", getAllUserById);

//Put a user by id
userRouter.put("/users/:userId", updateUserById);

//Delete a user by id
userRouter.delete("/users/:userId", deteleUserById);

//Export dữ liệu thành 1 module 
module.exports = userRouter;